var path = require('path');
var fs = require('fs');

//var config = JSON.parse(fs.readFileSync(path.join(__dirname, "/config.json"), 'utf8'));
var config = require('config');

var CONFIG = {};

CONFIG.ENV = (process.env.NODE_ENV || 'development');
CONFIG.PORT = (process.env.VCAP_APP_PORT || config.port);
CONFIG.DB_URL = 'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.database;
CONFIG.MOBILE_API = true; // true & false

CONFIG.DIRECTORY_USERS = './uploads/images/users/';


CONFIG.SECRET_KEY = '16f198404de4bb7b994f16b84e30f14f';

CONFIG.GCM_KEY = '';
CONFIG.GOOGLE_MAP_API_KEY = 'AIzaSyBdQiMiJA7oYVNgV9XIor4s9VUDi9keEAo';

CONFIG.SOCIAL_NETWORKS = {
    'facebookAuth': {
        'clientID': '1589589941346013',
        'clientSecret': 'ed67fd31ae628fde866bae1b6f71604e',
        'callbackURL': 'http://maidac.casperon.co/auth/facebook/callback'
      
    },
};

//Export Module
module.exports = CONFIG;
