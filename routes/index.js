var path = require("path");
var CONFIG = require('../config/config.js');
var jwt = require('jsonwebtoken');

function ensureAuthorized(req, res, next) {
    console.log("ssssssssssssss",req.headers);
    var token = req.headers.authorization;
    if (token) {
        jwt.verify(token, CONFIG.SECRET_KEY, function (err, decoded) {
            if (err) {
                res.send('Unauthorized Access');
            } else {
                next();
            }
        });
    } else {
        res.send('Unauthorized Access');
    }
}


module.exports = class BaseController {

    constructor(app,passport){
      this.app = app;
      this.passport = passport;  
      require('./auth.js')(passport);
      this.register();
    }

    register(){
    
    this.app.post('/login', this.passport.authenticate('loginuser', {
        successRedirect: '/success',
        failureRedirect: '/fails',
        failureFlash: true
    }));

    this.app.post('/register', this.passport.authenticate('registeruser', {
        successRedirect: '/success',
        failureRedirect: '/fails',
        failureFlash: true
    }));
    
    this.app.get('/success',function(req,res){
        console.log("req.session--->>",req.session);
        res.cookie('userdata', req.session.passport.user,{ expires: new Date(Date.now() + 90000), httpOnly: true });
        res.send({ user: req.session.passport.user.users.username, token: req.session.passport.user.token });
    })

    this.app.get('/getUsers',ensureAuthorized,function(req,res){
        res.send({"data":"success users"});
    });

    this.app.get('/UsersList',ensureAuthorized,function(req,res){
        res.send({"data":"UsersList users"});
    });

    this.app.get('/logout',function(req,res){
        req.session.destroy();
        res.send({ "msg":"logout successfully"});

    });

    this.app.get("/fails",function(req,res){
        res.cookie('userdata', 'wrong');
        res.send(req.session.flash.error);
    })

    this.app.get("*",function(req,res){
        console.log("ddddddd");
        res.sendFile(path.join(__dirname,'../dist/index.html'));
    })

    var user =require("../controller/user.js")();

    this.app.get("/getUserList",user.getUserList);
    this.app.post("/addUser",user.addUser)
    this.app.post("/getuserDoc",user  .getuserDoc);
    this.app.post("/editUser",user.editUser);
    }
}